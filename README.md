# MPD-GOODIES

A collection of commands to play with [MusicPlayerDaemon]:

  * Fading in/out over a specified time
  * Jumping to next album in queue.
  * Last modified artist/album/tracks
  * Crop the queue

Commands do not expose options to set MPD host|port, use MPD_HOST/MPD_PORT environment variables.  
MPD_HOST/MPD_PORT default to /run/mpd/socket or localhost:6600 when no environment variables are set (cf. [python-musicpd]).

[MusicPlayerDaemon]: https://www.musicpd.org/
[python-musicpd]: https://kaliko.gitlab.io/python-musicpd

[//]: # ( vim:set spelllang=en spell: )
