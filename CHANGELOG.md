mpd-goodies (0.1.0)

Initial release.

  * Convert previous code to python3
  * Switch to python-musicpd
  * Move to git

UNRELEASED
