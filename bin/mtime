#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2009,2010,2012,2019 kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import sys

from datetime import timedelta
from os.path import basename

import musicpd

VERSION = '0.2'


class Mtime(musicpd.MPDClient):
    script_info = dict({
        'prog': basename(__file__),
        'description': 'Print current playlist duration.',
        'epilog': 'Set MPD host/port in env. var'
    })

    def __init__(self):
        musicpd.MPDClient.__init__(self)
        self.args = self._get_args()
        self._run()

    def _get_args(self):
        parser = argparse.ArgumentParser(**self.__class__.script_info)
        parser.add_argument('--version', action='version',
                            version='v%s' % VERSION)
        parser.add_argument('-r', '--remaining', action='store_true', dest='remaining',
                            help='print remaining play time (only relevant when paused or playing).',)
        parser.add_argument('-H', '--human', action='store_true', dest='human',
                            help='print duration in human readable format.',)
        args = parser.parse_args()
        return args

    def _print_time(self, duration):
        if self.args.human:
            print(timedelta(seconds=duration))
        else:
            print(duration)

    def _run(self):
        self.connect()
        status = self.status()
        plinfo = self.playlistinfo()
        total_time = 0
        if self.args.remaining:
            if status.get('state') in ['play', 'pause']:
                total_time = sum(int(trk.get('time')) for trk in plinfo if int(
                    trk.get('pos')) > int(status.get('song')))
                # add remaining time from current song
                curr_elapsed = status.get('time').split(':')
                total_time += int(curr_elapsed[1]) - int(curr_elapsed[0])
        else:
            total_time = sum(int(trk.get('time')) for trk in plinfo)
        self._print_time(total_time)
        self.disconnect()
        sys.exit(0)


# Script starts here
if __name__ == '__main__':
    try:
        Mtime()
    except KeyboardInterrupt:
        sys.stdout.write('exit')

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
